package com.example.sharedpreferencesdwmo;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class BaseActivity extends Activity implements KeyConstants, OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}
	
	protected SharedPreferences getPreferenceObject(){
		return getSharedPreferences(Pref_Name, MODE_PRIVATE);
	}
	
	protected String getStringPrefs(String key, String defValue){
		return getPreferenceObject().getString(key, defValue);
	}
	protected boolean getBooleanPrefs(String key, boolean defValue){
		return getPreferenceObject().getBoolean(key, defValue);
	}
	protected void putStringPrefs(String key, String value){
		Editor editor = getPreferenceObject().edit();
		editor.putString(key, value);
		editor.commit();
	}
	protected void putBooleanPrefs(String key, boolean value){
		Editor editor = getPreferenceObject().edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Log.e("Current Method:", "BaseActivity : OnClick Method");
		
	}
	
	
}

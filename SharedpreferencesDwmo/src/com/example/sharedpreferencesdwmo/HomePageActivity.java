package com.example.sharedpreferencesdwmo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HomePageActivity extends BaseActivity {

	TextView userName;
	Button logout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_page);
		userName = (TextView) findViewById(R.id.userName);
		userName.setText(getStringPrefs(USERNAME, ""));
		logout = (Button) findViewById(R.id.logoutButton);
		logout.setOnClickListener(this);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_page, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Log.e("Current Method:", "HomepageActivity : OnClick Method");
		switch(arg0.getId()){
		case R.id.logoutButton:
			SharedPreferences prefs = getPreferenceObject();
			Editor editor = prefs.edit();
			editor.clear();
			editor.commit();
			startLoginActivity();
			break;
		}
	}
	
	
	private void startLoginActivity(){
		Intent i = new Intent(this, LoginActivity.class);
		startActivity(i);
		finish();
		
	}

}

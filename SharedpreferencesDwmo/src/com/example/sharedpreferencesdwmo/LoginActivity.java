package com.example.sharedpreferencesdwmo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends BaseActivity {

	EditText username, password;
	Button loginButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		boolean flag = getBooleanPrefs(isUserLoggedIn, false);
		if (flag) {
			startHomePageActivity();
		}

		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);
		loginButton = (Button) findViewById(R.id.loginButton);
		loginButton.setOnClickListener(this);
	}

	private void startHomePageActivity() {
		Intent i = new Intent(this, HomePageActivity.class);
		startActivity(i);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		Log.e("Current Method:", "LoginActivity : OnClick Method");
		switch (v.getId()) {
		case R.id.loginButton:
			if (isUserAuthorized()) {
				putBooleanPrefs(isUserLoggedIn, true);
				putStringPrefs(USERNAME, username.getText().toString());
				putStringPrefs(PASSWORD, password.getText().toString());
				startHomePageActivity();
			}else{
				// Show Toast
			}
			break;
		}
	}

	private boolean isUserAuthorized() {
		String user = username.getText().toString();
		String pwd = password.getText().toString();

		if (user.equalsIgnoreCase("USER") && pwd.equalsIgnoreCase("password")) {
			return true;
		}
		return false;

	}

}
